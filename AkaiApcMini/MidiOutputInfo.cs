namespace AkaiApcMini;

public record class MidiOutputInfo(int Index, MidiOutCapabilities Info);
