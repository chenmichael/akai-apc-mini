namespace AkaiApcMini;

public class MidiDeviceManager(ILogger<MidiDeviceManager> logger, ILoggerFactory loggerFactory) : IMidiDeviceManager {
    private readonly ILogger<MidiDeviceManager> Logger = logger;
    private readonly ILoggerFactory loggerFactory = loggerFactory;

    public static IEnumerable<MidiInputInfo> EnumerateInputDevices() {
        for (var i = 0; i < MidiIn.NumberOfDevices; i++)
            yield return new(i, MidiIn.DeviceInfo(i));
    }

    public static IEnumerable<MidiInputInfo> EnumerateInputDevices<TDevice>() where TDevice : IMidiDevice<TDevice>
        => EnumerateInputDevices().Where(i => i.Info.ProductName == TDevice.Name);

    public static IEnumerable<MidiOutputInfo> EnumerateOutputDevices() {
        for (var i = 0; i < MidiOut.NumberOfDevices; i++)
            yield return new(i, MidiOut.DeviceInfo(i));
    }

    public static IEnumerable<MidiOutputInfo> EnumerateOutputDevices<TDevice>() where TDevice : IMidiDevice<TDevice>
        => EnumerateOutputDevices().Where(i => i.Info.ProductName == TDevice.Name);

    private static MidiIn GetMidiIn<TDevice>(int offset = 0) where TDevice : IMidiDevice<TDevice> {
        var availableDevices = EnumerateInputDevices<TDevice>().ToList();
        var device = availableDevices
            .Skip(offset)
            .FirstOrDefault();
        return device is null
            ? throw new InvalidProgramException($"Input device {TDevice.Name} [Nr. {offset}] is not connected! Available: {string.Join(" ", availableDevices.Select(i => i.Info.ProductName))}") : new MidiIn(device.Index);
    }

    public static int NumberOfConnected<TDevice>() where TDevice : IMidiDevice<TDevice>
        => EnumerateInputDevices<TDevice>().Count();

    private static MidiOut GetMidiOut<TDevice>(int offset = 0) where TDevice : IMidiDevice<TDevice> {
        var availableDevices = EnumerateOutputDevices<TDevice>().ToList();
        var device = availableDevices
            .Skip(offset)
            .FirstOrDefault();
        return device is null
            ? throw new InvalidProgramException($"Output device {TDevice.Name} [Nr. {offset}] is not connected! Available: {string.Join(" ", availableDevices.Select(i => i.Info.ProductName))}") : new MidiOut(device.Index);
    }

    public TDevice CreateDevice<TDevice>(int offset = 0) where TDevice : IMidiDevice<TDevice> {
        var input = GetMidiIn<TDevice>(offset);
        var output = GetMidiOut<TDevice>(offset);
        var device = TDevice.CreateDevice(loggerFactory, 1, input, output);
        Logger.LogInformation("Created '{name}' device with offset {offset}!", TDevice.Name, offset);
        return device;
    }
}
