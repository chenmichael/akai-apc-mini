namespace AkaiApcMini;

public record class MidiInputInfo(int Index, MidiInCapabilities Info);
