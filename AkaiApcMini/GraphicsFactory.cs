using AkaiApcMini.Display;

namespace AkaiApcMini;
public class GraphicsFactory(ILogger<GraphicsFactory> logger, ILoggerFactory loggerFactory) {
    public Graphics<TColor> CreateGraphics<TColor>(IDisplay<TColor> display) {
        logger.LogDebug("Creating graphics for display {display} using color space {colors}!", display.GetType().Name, typeof(TColor).Name);
        return new(loggerFactory.CreateLogger<Graphics<TColor>>(), display);
    }
}
