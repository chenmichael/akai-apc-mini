using AkaiApcMini.Controls;

namespace AkaiApcMini;

public class StopButtonCollection(ILoggerFactory loggerFactory, ApcMini controller) : ControlSet<Button<ApcMini>, ApcMini>(loggerFactory, controller, 64, 8) { }
