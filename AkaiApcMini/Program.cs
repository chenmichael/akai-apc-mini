using AkaiApcMini;
using AkaiApcMini.Controls;
using AkaiApcMini.Display;
using AkaiApcMini.Games;

var provider = new ServiceProvider();
var logger = provider.GetService<ILogger<Program>>();
var devices = provider.GetService<IMidiDeviceManager>();
var graphicsFactory = provider.GetService<GraphicsFactory>();

var cts = new CancellationTokenSource();
Console.CancelKeyPress += Console_CancelKeyPress;

void Console_CancelKeyPress(object? sender, ConsoleCancelEventArgs e) {
    logger.LogInformation("Cancellation requested from console!");
    e.Cancel = true;
    cts.Cancel();
}

using var apc = devices.CreateDevice<ApcMini>();

try {
    var graphics = graphicsFactory.CreateGraphics(apc.Matrix);
    var game = provider.GetService<GameMenu>();
    using (game.RunWith(apc))
        await GameLoop(graphics, game, cts.Token);

    graphics.Fill(ButtonColor.Off);
    graphics.Flush();
} catch (Exception e) {
    logger.LogCritical(e, "Uncaught exception in entry point!");
    throw;
} finally {
    apc.Dispose();
}

static async Task GameLoop<TColor>(Graphics<TColor> graphics, ISyncGame<TColor> game, CancellationToken ct) {
    try {
        while (true) {
            await game.WaitForFrameAsync(ct);
            game.Draw(graphics);
            graphics.Flush();
        }
    } catch (OperationCanceledException) { }
}
