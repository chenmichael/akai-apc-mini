using AkaiApcMini.Controls;
using AkaiApcMini.Display;

namespace AkaiApcMini;

public class ApcMini : IMidiDevice<ApcMini>, IDisposable, IInputDevice {
    public static ApcMini CreateDevice(ILoggerFactory loggerFactory, int channel, MidiIn input, MidiOut output)
        => new(loggerFactory, channel, input, output);

    public ApcMini(ILoggerFactory loggerFactory, int channel, MidiIn input, MidiOut output) {
        Logger = loggerFactory.CreateLogger<ApcMini>();
        Matrix = new(this, loggerFactory.CreateLogger<BufferedMatrixDisplay>());
        StopButtons = new(this);
        ControlButtons = new(this);
        Channel = channel;
        Input = input;
        Output = output;
        ButtonDown += ApcMini_ButtonDown;
        FaderMoved += ApcMini_FaderMoved;
        input.MessageReceived += MessageReceived;
        input.Start();
    }

    public static string Name => "APC MINI";

    private readonly ILogger<ApcMini> Logger;

    public int Channel { get; }
    public MidiIn Input { get; }
    public MidiOut Output { get; }

    #region explicit IDisplay
    private void SetColor(int button, int color)
        => Output.Send(new NoteOnEvent(0L, Channel, button, color, 0).GetAsShortMessage());

    public abstract class MidiDisplay<TColor>(int width, int height, ApcMini apc, int offset) : IDisplay<TColor> {
        public int Width => width;
        public int Height => height;
        public void SetColor(int x, int y, TColor color) {
            var button = offset + x + (y * Width);
            apc.SetColor(button, GetColorCode(color));
        }
        protected abstract int GetColorCode(TColor color);
    }

    public BufferedMatrixDisplay Matrix { get; }
    public class MatrixDisplay(ApcMini apc) : MidiDisplay<ButtonColor>(8, 8, apc, 0) {
        protected override int GetColorCode(ButtonColor color) => (int)color;
    }
    public class BufferedMatrixDisplay(ApcMini apc, ILogger<BufferedMatrixDisplay> logger) : BufferedDisplay<ButtonColor>(logger, new MatrixDisplay(apc)) { }

    public abstract class LightButtonBar<TColor>(int length, ApcMini apc, int offset) {
        public int Length { get; } = length;
        public void SetColor(int index, TColor color) => apc.SetColor(offset + index, GetColorCode(color));
        protected abstract int GetColorCode(TColor color);
    }
    public class LightButtonBar(int length, ApcMini apc, int offset) : LightButtonBar<OnOffColor>(length, apc, offset) {
        protected override int GetColorCode(OnOffColor color) => (int)color;
    }

    public StopButtonDisplay StopButtons { get; }
    public class StopButtonDisplay(ApcMini apc) : LightButtonBar(8, apc, 64);

    public ControlButtonDisplay ControlButtons { get; }
    public class ControlButtonDisplay(ApcMini apc) : LightButtonBar(8, apc, 82);
    #endregion

    public event ButtonDownEventHandler ButtonDown;
    public event FaderMovedEventHandler FaderMoved;

    private void MessageReceived(object? sender, MidiInMessageEventArgs e) {
        switch (e.MidiEvent) {
        case NoteOnEvent on: ButtonDown.Invoke(on.Channel, on.NoteNumber); break;
        case NoteEvent note: Logger.LogDebug("[Channel {channel}]: Note {id} with command {command}!", note.Channel, note.NoteNumber, note.CommandCode); break;
        case ControlChangeEvent change: FaderMoved.Invoke(change.Channel, (int)change.Controller, change.ControllerValue); break;
        case var msg: Logger.LogWarning("Unhandled midi event received of type {type}", msg.GetType()); break;
        };
    }

    private void ApcMini_FaderMoved(int channel, int id, int value)
        => Logger.LogDebug("[Channel {channel}]: Fader {id} was set to {value}!", channel, id, value);
    private void ApcMini_ButtonDown(int channel, int id)
        => Logger.LogDebug("[Channel {channel}]: Note {id} pressed down!", channel, id);

    private bool isDisposed = false;

    private void Dispose(bool disposing) {
        if (isDisposed) return;
        if (disposing) {
            Input.Stop();
            Input.Dispose();
            Output.Dispose();
        }

        isDisposed = true;
    }

    public void Dispose() {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
