namespace AkaiApcMini;

public interface IMidiDeviceManager {
    TDevice CreateDevice<TDevice>(int offset = 0) where TDevice : IMidiDevice<TDevice>;
}
