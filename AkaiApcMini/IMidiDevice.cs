namespace AkaiApcMini;

public interface IMidiDevice<TSelf> where TSelf : IMidiDevice<TSelf> {
    static abstract string Name { get; }
    static abstract TSelf CreateDevice(ILoggerFactory loggerFactory, int channel, MidiIn input, MidiOut output);

    MidiIn Input { get; }
    MidiOut Output { get; }
}
