using AkaiApcMini.Games;

namespace AkaiApcMini.Controls;

public static class InputConnectorExtensions {
    public static IDisposable WithInputDevice(this IInputHandler handler, IInputDevice device)
        => new InputConnector(handler, device);
    public static IDisposable RunWith<TColor>(this ISyncGame<TColor> game, IInputDevice device)
        => new GameRunnerEnvironment<TColor>(game, game.WithInputDevice(device));
}
