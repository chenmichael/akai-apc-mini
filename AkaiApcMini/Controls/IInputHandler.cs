namespace AkaiApcMini.Controls;

public interface IInputHandler {
    void ButtonDown(int channel, int id);
    void FaderInput(int channel, int id, int value);
}
