namespace AkaiApcMini.Controls;

public interface IInputDevice
    : IButtonInput
    , IFaderInput { }

public interface IButtonInput {
    event ButtonDownEventHandler ButtonDown;
}

public interface IFaderInput {
    event FaderMovedEventHandler FaderMoved;
}

public delegate void ButtonDownEventHandler(int channel, int id);
public delegate void FaderMovedEventHandler(int channel, int id, int value);
