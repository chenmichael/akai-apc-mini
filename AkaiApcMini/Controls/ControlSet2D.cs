using System.Collections;

namespace AkaiApcMini.Controls;

public abstract class ControlSet2D<TControl, TDevice>(ILoggerFactory loggerFactory, TDevice controller, int start, int width, int height)
    : IReadOnlyCollection<TControl>
    where TControl : IControlElement<TControl, TDevice>
    where TDevice : IMidiDevice<TDevice> {
    private readonly TControl[,] controls = InitFaders(loggerFactory, controller, start, width, height);

    public int Count => controls.Length;

    private static TControl[,] InitFaders(ILoggerFactory loggerFactory, TDevice controller, int start, int width, int height) {
        var buttons = new TControl[width, height];
        for (var x = 0; x < width; x++)
            for (var y = 0; y < height; y++)
                buttons[x, y] = TControl.FromController(loggerFactory.CreateLogger<TControl>(), controller, start + x + (width * y));
        return buttons;
    }

    private IEnumerable<TControl> Enumerate() {
        foreach (var item in controls)
            yield return item;
    }
    public IEnumerator<TControl> GetEnumerator() => Enumerate().GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    public TControl this[Index x, Index y] => controls[x.GetOffset(width), y.GetOffset(height)];
}
