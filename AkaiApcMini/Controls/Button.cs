namespace AkaiApcMini.Controls;

public class Button<TDevice> : ControlElementBase<TDevice>, IControlElement<Button<TDevice>, TDevice>
    where TDevice : IMidiDevice<TDevice> {
    public event EventHandler<bool>? Pressed;

    public static Button<TDevice> FromController(ILogger<Button<TDevice>> logger, TDevice controller, int channel) => new(logger, controller, channel);

    public Button(ILogger<Button<TDevice>> logger, TDevice controller, int channel) : base(logger, controller, channel) =>
        //controller.RegisterButton(this, channel);
        Pressed += Button_Pressed;

    private void Button_Pressed(object? sender, bool e)
        => Logger.LogDebug("Button {note} was pressed {dir}!", Channel, e ? "down" : "up");
    public void Press() => Pressed?.Invoke(this, true);
}

public class ColorButton(ILogger<ColorButton> logger, ApcMini controller, int channel) : Button<ApcMini>(logger, controller, channel), IControlElement<ColorButton, ApcMini> {
    public static ColorButton FromController(ILogger<ColorButton> logger, ApcMini controller, int channel) => new(logger, controller, channel);
    public void SetColor(ButtonColor color)
        => controller.Output.Send(new NoteOnEvent(0L, 1, Channel, (int)color, 0).GetAsShortMessage());
}

public enum ButtonColor {
    Off,
    Green,
    GreenFlashing,
    Red,
    RedFlashing,
    Yellow,
    YellowFlashing,
}

public enum OnOffColor {
    Off,
    On,
    Flashing,
}
