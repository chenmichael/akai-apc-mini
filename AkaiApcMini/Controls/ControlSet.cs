using System.Collections;

namespace AkaiApcMini.Controls;

public abstract class ControlSet<TControl, TDevice>(ILoggerFactory loggerFactory, TDevice controller, int start, int count)
    : IReadOnlyCollection<TControl>
    where TControl : IControlElement<TControl, TDevice>
    where TDevice : IMidiDevice<TDevice> {
    private readonly TControl[] controls = Enumerable.Range(start, count)
        .Select(i => TControl.FromController(loggerFactory.CreateLogger<TControl>(), controller, i))
        .ToArray();

    public TControl this[Index index] => controls[index];

    public int Count => controls.Length;

    public IEnumerator<TControl> GetEnumerator() => ((IEnumerable<TControl>)controls).GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}
