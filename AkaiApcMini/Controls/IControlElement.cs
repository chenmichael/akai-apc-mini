namespace AkaiApcMini.Controls;

public interface IControlElement<TSelf, TDevice>
    where TSelf : IControlElement<TSelf, TDevice>
    where TDevice : IMidiDevice<TDevice> {
    static abstract TSelf FromController(ILogger<TSelf> logger, TDevice controller, int channel);
}
