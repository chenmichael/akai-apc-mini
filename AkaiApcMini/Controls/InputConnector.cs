namespace AkaiApcMini.Controls;

/// <summary>
/// Provides a context manager for connecting an input device to a handler.
/// </summary>
public class InputConnector : IDisposable {
    private readonly IInputHandler game;
    private readonly IInputDevice device;

    public InputConnector(IInputHandler handler, IInputDevice device) {
        game = handler;
        this.device = device;
        RegisterInput();
    }

    #region Input
    private void RegisterInput() {
        device.ButtonDown += Device_ButtonDown;
        device.FaderMoved += Device_FaderMoved;
    }

    private void UnregisterInput() {
        device.ButtonDown -= Device_ButtonDown;
        device.FaderMoved -= Device_FaderMoved;
    }

    private void Device_FaderMoved(int channel, int id, int value) => game.FaderInput(channel, id, value);
    private void Device_ButtonDown(int channel, int id) => game.ButtonDown(channel, id);
    #endregion

    #region Disposing
    private bool isDisposed;

    protected virtual void Dispose(bool disposing) {
        if (isDisposed) return;
        if (disposing)
            UnregisterInput();

        isDisposed = true;
    }

    public void Dispose() {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
