namespace AkaiApcMini.Controls;

public class Fader<TDevice> : ControlElementBase<TDevice>, IControlElement<Fader<TDevice>, TDevice>
    where TDevice : IMidiDevice<TDevice> {
    public event EventHandler<int>? LevelChanged;

    public Fader(ILogger<Fader<TDevice>> logger, TDevice controller, int channel) : base(logger, controller, channel) =>
        //controller.RegisterFader(this, channel);
        LevelChanged += Fader_LevelChanged;

    private void Fader_LevelChanged(object? sender, int e)
        => Logger.LogDebug("Control {note} was set to {vel}!", Channel, e);
    public static Fader<TDevice> FromController(ILogger<Fader<TDevice>> logger, TDevice controller, int channel) => new(logger, controller, channel);
    public void ValueUpdated(int controllerValue) => LevelChanged?.Invoke(this, controllerValue);
}
