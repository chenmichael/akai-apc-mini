namespace AkaiApcMini.Controls;

public class ChannelFaderCollection(ILoggerFactory loggerFactory, ApcMini controller) : ControlSet<Fader<ApcMini>, ApcMini>(loggerFactory, controller, 48, 8) { }
