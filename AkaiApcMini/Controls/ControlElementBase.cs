namespace AkaiApcMini.Controls;

public abstract class ControlElementBase<TDevice>(ILogger<ControlElementBase<TDevice>> Logger, IMidiDevice<TDevice> controller, int channel)
    where TDevice : IMidiDevice<TDevice> {
    protected readonly ILogger<ControlElementBase<TDevice>> Logger = Logger;
    protected readonly IMidiDevice<TDevice> controller = controller;

    public int Channel { get; } = channel;
}
