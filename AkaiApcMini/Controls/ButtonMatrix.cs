namespace AkaiApcMini.Controls;

public class ButtonMatrix(ILoggerFactory loggerFactory, ApcMini controller) : ControlSet2D<ColorButton, ApcMini>(loggerFactory, controller, 0, 8, 8) {
    public void SetColor(ButtonColor color) {
        foreach (var item in this)
            item.SetColor(color);
    }
}
