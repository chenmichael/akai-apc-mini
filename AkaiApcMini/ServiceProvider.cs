using AkaiApcMini.Controls;
using AkaiApcMini.Games;
using Jab;

namespace AkaiApcMini;

[ServiceProvider]
[Singleton<ILoggerFactory>(Instance = nameof(LoggerFactoryProvider))]
[Singleton(typeof(ILogger<>), typeof(Logger<>))]
[Singleton<GraphicsFactory>]
[Singleton<IMidiDeviceManager, MidiDeviceManager>]
[Singleton<GameMenu>]
[Singleton<ISyncGame<ButtonColor>, Checkers>]
[Singleton<ISyncGame<ButtonColor>, SnakeGame>]
[Singleton<ISyncGame<ButtonColor>, PongGame>]
[Singleton<ISyncGame<ButtonColor>, TicTacToe>]
[Singleton<ISyncGame<ButtonColor>, ConnectFour>]
[Singleton<ISyncGame<ButtonColor>, AudioVisualizer>]
public partial class ServiceProvider {
    public ILoggerFactory LoggerFactoryProvider { get; } = LoggerFactory.Create(ConfigureLogging);
    private static void ConfigureLogging(ILoggingBuilder builder) => builder
        .AddSimpleConsole(options => options.SingleLine = true)
        .SetMinimumLevel(LogLevel.Information);
}
