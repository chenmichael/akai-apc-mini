using AkaiApcMini.Controls;
using AkaiApcMini.Display;

namespace AkaiApcMini.Games;

public class ConnectFour(ILogger<ConnectFour> logger) : MnkGame<ConnectFour.Dimensions, ButtonColor>(logger, 4) {
    public class Dimensions : IFixedDimensions {
        public static int Width => 8;
        public static int Height => 7;
    }

    #region Drawing
    public override void Draw(Graphics<ButtonColor> graphics) {
        graphics.Fill(ButtonColor.Off);
        DrawGrid(graphics, State.Grid);

        // Draw winner
        if (State.WinningGroup is { } group)
            foreach (var field in group) {
                var player = State.Grid[field.X, field.Y] ?? throw new InvalidProgramException("Winning group contains invalid field!");
                graphics.SetColor(field, PlayerColor(player, true));
            }

        // Indicate active player if game is active
        if (State.WinningGroup is null)
            DrawActivePlayer(graphics, State.ActivePlayer);
    }

    private void DrawActivePlayer(Graphics<ButtonColor> graphics, bool player) {
        var color = PlayerColor(player, true);
        graphics.DrawHorizontalLine(Height, 0, Width, color);
    }

    private void DrawGrid(Graphics<ButtonColor> graphics, bool?[,] grid) {
        for (var y = 0; y < Height; y++)
            for (var x = 0; x < Width; x++)
                if (grid[x, y] is { } player)
                    graphics.SetColor(x, y, PlayerColor(player));
    }

    private static ButtonColor PlayerColor(bool player, bool winning = false) => winning
        ? (player ? ButtonColor.RedFlashing : ButtonColor.GreenFlashing)
        : (player ? ButtonColor.Red : ButtonColor.Green);

    private static string PlayerName(bool player)
        => PlayerColor(player).ToString();
    #endregion

    #region Logic
    protected override void FieldSelected(int x, int y) {
        if (y != Height) return;

        if (!IsFree(x, out y)) {
            logger.LogWarning("Player {player} chose full column {x}!", PlayerName(State.ActivePlayer), x);
            return;
        }

        ref var field = ref State.Grid[x, y];
        if (field is { })
            throw new InvalidProgramException("Is free returned true, but the field is not free!");

        logger.LogInformation("Player {player} chose column {x}!", PlayerName(State.ActivePlayer), x);

        field = State.ActivePlayer;
        State.ActivePlayer = !State.ActivePlayer;
    }

    private bool IsFree(int x, out int y) {
        for (y = 0; y < Height; y++)
            if (State.Grid[x, y] is null)
                return true;
        return false;
    }
    #endregion
}
