using System.Diagnostics;
using AkaiApcMini.Controls;
using AkaiApcMini.Display;

namespace AkaiApcMini.Games;

public class Checkers(ILogger<Checkers> logger) : SyncGameBase<Checkers.GameState, ButtonColor>(logger) {
    // Game config
    public const int SIZE = 8;

    public class GameState : IGameState<GameState> {
        [DebuggerDisplay($"{{{nameof(Name)},nq}}")]
        public struct Piece {
            public bool Player;
            public bool IsKing;

            public readonly ButtonColor Color => IsKing
                ? (Player ? ButtonColor.RedFlashing : ButtonColor.GreenFlashing)
                : (Player ? ButtonColor.Red : ButtonColor.Green);
            public readonly bool IsMan => !IsKing;
            public readonly string Name => $"{PlayerName(Player)} {(IsKing ? "King" : "Man")}";

            public static string PlayerName(bool player) => player ? "Red" : "Green";
        }

        public Piece?[,] Grid = new Piece?[SIZE, SIZE];
        public Coordinate<int>? SelectedField;
        public Coordinate<int>? MovedField;
        public bool ActivePlayer;
        public bool BeginnerPlayer;
        public bool? Winner;

        public void Reset() {
            ClearGrid();
            ActivePlayer = BeginnerPlayer = false;
        }

        public void ClearGrid() {
            Array.Clear(Grid, 0, Grid.Length);
            SetGrid();
        }

        private void SetGrid() {
            for (var y = 0; y < 3; y++)
                SetRow(y, false);
            for (var y = SIZE - 3; y < SIZE; y++)
                SetRow(y, true);
        }

        private void SetRow(int y, bool player) {
            for (var x = y % 2; x < SIZE; x += 2)
                Grid[x, y] = new() {
                    Player = player,
                    IsKing = false,
                };
        }

        public static GameState Create() => new();
    }

    #region Drawing
    public override void Draw(Graphics<ButtonColor> graphics) {
        graphics.Fill(ButtonColor.Off);

        for (var y = 0; y < SIZE; y++)
            for (var x = 0; x < SIZE; x++)
                if (State.Grid[x, y] is { } piece)
                    graphics.SetColor(x, y, (State.Winner is { } winner && winner == piece.Player)
                        ? (piece with { IsKing = true }).Color
                        : piece.Color);

        if (State.Winner is null)
            if (State.SelectedField is { } field)
                if (PieceOn(field) is { }) {
                    graphics.SetColor(field.X, field.Y, ButtonColor.Yellow);
                    HighlightPossibleMoves(graphics, field);
                } else logger.LogWarning("Selected field ({space}) does not contain any pieces!", field);
    }

    private void HighlightPossibleMoves(Graphics<ButtonColor> graphics, Coordinate<int> field) {
        // Highlight possible moves
        var pos = new Coordinate<int>();
        for (pos.Y = 0; pos.Y < SIZE; pos.Y++)
            for (pos.X = 0; pos.X < SIZE; pos.X++)
                if (CanMove(field, pos, out _))
                    graphics.SetColor(pos.X, pos.Y, ButtonColor.YellowFlashing);
    }
    #endregion

    #region Logic
    public override void ButtonDown(int channel, int id) {
        var y = id / 8;
        if (y is not >= 0 or not < 8) return; // Ignore buttons outside matrix
        var x = id % 8;
        var clicked = new Coordinate<int>(x, y);

        if (State.Winner is not null) {
            Reset();
            return;
        }

        if (State.MovedField is { } movedField) {
            if (movedField != clicked) {
                TryMove(movedField, clicked);
            } else {
                logger.LogInformation("Player {player} ended his turn!", GameState.Piece.PlayerName(State.ActivePlayer));
                ChangeTurns();
            }
        } else {
            if (State.Grid[x, y] is { } piece)
                // Piece was selected
                State.SelectedField = piece.Player == State.ActivePlayer ? clicked : null;
            else if (State.SelectedField is not { } selected) return;
            else {
                // Empty spot was selected with selected piece
                State.SelectedField = null;
                TryMove(selected, clicked);
            }
        }

        RequestAnimation();
    }

    private void ChangeTurns() {
        State.MovedField = null;
        State.SelectedField = null;
        var oldPlayer = State.ActivePlayer;
        State.ActivePlayer = !oldPlayer;
        logger.LogInformation("Player {prev}'s turn ended and player {new} is now playing!", GameState.Piece.PlayerName(oldPlayer), GameState.Piece.PlayerName(State.ActivePlayer));
    }

    private ref GameState.Piece? PieceOn(Coordinate<int> coordinate)
        => ref State.Grid[coordinate.X, coordinate.Y];

    private bool CanMove(Coordinate<int> field, Coordinate<int> pos, out Coordinate<int>? enemyLocation)
        => CannotMove(field, pos, out enemyLocation) is null;

    private string? CannotMove(Coordinate<int> selected, Coordinate<int> clicked, out Coordinate<int>? enemyLocation) {
        enemyLocation = null;

        if (!IsBlackField(clicked))
            return "the target field is not a black field";
        ref var selectedField = ref PieceOn(selected);
        if (selectedField is not { } selectedPiece)
            return "the selected field does not contain a piece";
        if (selectedPiece.Player != State.ActivePlayer) {
            return $"the selected piece ({selectedPiece.Name}) cannot be moved by active player {GameState.Piece.PlayerName(State.ActivePlayer)}";
        } // allow moves by active player
        var distance = clicked - selected;
        var absX = Math.Abs(distance.X);
        var absY = Math.Abs(distance.Y);

        if (absX != absY)
            return "only allow diagonal moves are allowed";
        if (absX is 0)
            return "a piece must be moved to a different position than it's current";

        var direction = distance;
        direction.X /= absX;
        direction.Y /= absY;

        if (!selectedPiece.IsKing)
            // Men can only jump forwards
            if (selectedPiece.Player ? direction.Y is >= 0 : direction.Y is <= 0)
                return "men cannot be moved backwards, only kings can";

        ref var clickedField = ref PieceOn(clicked);
        if (clickedField is { } clickedPiece)
            return $"the target field is already occupied by a {clickedPiece.Name}";

        if (absX is not 1) {
            if (selectedPiece.IsMan && absX > 2)
                return "men can only move 1 field or 2 if an enemy is captured";
            var pos = selected;
            for (var i = 1; i < absX; i++)
                if (PieceOn(pos += direction) is { } piece) {
                    // Cannot skip own pieces
                    if (piece.Player == State.ActivePlayer)
                        return "pieces cannot skip over their own pieces";
                    // Can only skip a single enemy
                    if (enemyLocation is { } location)
                        return $"pieces cannot skip over multiple enemies at {location} and {pos}!";
                    else enemyLocation = pos;
                }

            // Men can only skip if there is an enemy
            if (enemyLocation is null)
                if (selectedPiece.IsMan)
                    return "men can only move 2 fields if an enemy is captured";
        }


        if (State.MovedField is not null)
            if (enemyLocation is null)
                return "pieces can only do multiple moves if a piece is captured every move";

        return null;
    }

    private void TryMove(Coordinate<int> selected, Coordinate<int> clicked) {
        if (CannotMove(selected, clicked, out var enemyLocation) is { } reason) {
            logger.LogWarning("Piece at {selected} cannot move to {clicked}: {reason}", selected, clicked, reason);
            return;
        }
        ExecuteMove(selected, clicked, enemyLocation);
    }

    private void ExecuteMove(Coordinate<int> selected, Coordinate<int> clicked, Coordinate<int>? enemyLocation) {
        ref var selectedField = ref PieceOn(selected);
        ref var clickedField = ref PieceOn(clicked);

        if (selectedField is not { } selectedPiece)
            throw new InvalidProgramException("Piece from empty spot is trying to move!");

        var endLine = State.ActivePlayer ? 0 : SIZE - 1;

        // Crown kings
        selectedField = clickedField;
        if (clicked.Y == endLine) {
            var crowned = selectedPiece with { IsKing = true };
            clickedField = crowned;
            logger.LogInformation("{piece} moved from {position} to {newPosition} and was crowned!", crowned.Name, selected, clicked);
        } else {
            clickedField = selectedPiece;
            logger.LogInformation("{piece} moved from {position} to {newPosition}", selectedPiece.Name, selected, clicked);
        }

        // Select new field for chained moves

        // Take enemy if skipped
        if (enemyLocation is { } enemyPos) {
            ref var captured = ref PieceOn(enemyPos);
            if (captured is not { } capturedPiece)
                throw new InvalidProgramException("Captured piece location was empty!");
            logger.LogInformation("Player captured enemy {piece} with his {capturer}", capturedPiece.Name, selectedPiece.Name);
            captured = default;
            State.SelectedField = clicked;
            State.MovedField = clicked;

            CheckGameOver();
        } else
            // If no enemy was taken, the turn is over
            ChangeTurns();
    }

    private void CheckGameOver() {
        if (GetWinner() is { } winner) {
            State.Winner = winner;
            logger.LogInformation("Player {player} has won the game!", GameState.Piece.PlayerName(winner));
        }
    }

    private bool? GetWinner() {
        var p1Count = 0;
        var p2Count = 0;
        var pos = new Coordinate<int>();
        for (pos.Y = 0; pos.Y < SIZE; pos.Y++)
            for (pos.X = 0; pos.X < SIZE; pos.X++)
                if (PieceOn(pos) is { } piece)
                    if (piece.Player) p1Count++;
                    else p2Count++;
        return p1Count is 0 ? true : p2Count is 0 ? false : null;
    }

    private static bool IsBlackField(Coordinate<int> clicked)
        => (clicked.X + clicked.Y) % 2 == 0;
    #endregion
}
