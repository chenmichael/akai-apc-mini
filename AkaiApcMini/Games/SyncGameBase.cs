using AkaiApcMini.Display;

namespace AkaiApcMini.Games;

public abstract class SyncGameBase<TState, TColor> : ISyncGame<TColor>
    where TState : IGameState<TState> {
    private readonly ILogger<SyncGameBase<TState, TColor>> logger;
    protected readonly TState State = TState.Create();
    protected readonly SemaphoreSlim semaphore = new(0);
    private int frameCount = 0;
    private bool running = false;

    public event UpdateHandler<TColor> UpdateAvailable;

    public SyncGameBase(ILogger<SyncGameBase<TState, TColor>> logger) {
        this.logger = logger;
        UpdateAvailable += GameBase_UpdateAvailable;
        Reset();
    }

    private void GameBase_UpdateAvailable(ISyncGame<TColor> sender) {
        logger.LogInformation("Frame update {number} is available!", frameCount++);
        _ = semaphore.Release();
    }

    public async Task WaitForFrameAsync(CancellationToken token) {
        logger.LogInformation("Waiting for animation frame!");
        await semaphore.WaitAsync(token);
        logger.LogInformation("Animation frame {frame} received!", frameCount);
    }

    protected void RequestAnimation() {
        logger.LogDebug("Animation frame was requested!");
        UpdateAvailable.Invoke(this);
    }

    private readonly object runStateLock = new();
    public bool Running {
        get => running; private set {
            lock (runStateLock) {
                if (running == value) return;
                RunningStateChanged(running = value);
            }
            logger.LogInformation("Game was {status}!", value ? "started" : "paused");
            if (value) RequestAnimation();
        }
    }

    public void Reset() {
        State.Reset();
        logger.LogInformation("Game state was reset!");
        RequestAnimation();
    }

    public void Start() => Running = true;
    public void Stop() => Running = false;

    public virtual void ButtonDown(int channel, int id) { }
    public virtual void FaderInput(int channel, int id, int value) { }
    protected virtual void RunningStateChanged(bool running) { }

    public abstract void Draw(Graphics<TColor> display);
}
