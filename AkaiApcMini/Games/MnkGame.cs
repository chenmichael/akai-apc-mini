using System.Diagnostics.CodeAnalysis;

namespace AkaiApcMini.Games;

public class MnkState<TDimensions> : IGameState<MnkState<TDimensions>>
    where TDimensions : IFixedDimensions {

    public bool?[,] Grid = new bool?[TDimensions.Width, TDimensions.Height];
    public bool ActivePlayer;
    public bool BeginnerPlayer;
    public Coordinate<int>[]? WinningGroup;

    public void Reset() {
        ClearGrid();
        ActivePlayer = BeginnerPlayer = false;
        WinningGroup = null;
    }

    public void ClearGrid() => Array.Clear(Grid, 0, Grid.Length);
    public static MnkState<TDimensions> Create() => new();
}

public interface IFixedDimensions {
    static abstract int Width { get; }
    static abstract int Height { get; }
}

public abstract class MnkGame<TDimensions, TColor>(ILogger<MnkGame<TDimensions, TColor>> logger, int inaRow) : SyncGameBase<MnkState<TDimensions>, TColor>(logger)
    where TDimensions : IFixedDimensions {
    public int Width => TDimensions.Width;
    public int Height => TDimensions.Height;

    #region Logic
    public override void ButtonDown(int channel, int id) {
        if (State.WinningGroup is not null) {
            // If winner is determined a push on the matrix restarts the game
            State.WinningGroup = null;
            State.ClearGrid();
            // Next rounds starts with other player
            State.BeginnerPlayer = !State.BeginnerPlayer;
            State.ActivePlayer = State.BeginnerPlayer;

            // Push only clears the grid, doesn't set new game
            RequestAnimation();
            return;
        }

        const int MATRIXSIZE = 8;
        var y = id / MATRIXSIZE;
        if (y is < 0 or >= MATRIXSIZE) return;
        var x = id % MATRIXSIZE;
        if (x is < 0 or >= MATRIXSIZE) return;

        FieldSelected(x, y);

        if (IsGameOver(State.Grid, out var winner))
            State.WinningGroup = winner;

        RequestAnimation();
    }

    protected abstract void FieldSelected(int x, int y);

    private bool IsGameOver(bool?[,] grid, [MaybeNullWhen(false)] out Coordinate<int>[] winner) {
        winner = new Coordinate<int>[inaRow];
        for (var y = 0; y < Height; y++) {
            var yFits = y + inaRow <= Height;
            for (var x = 0; x < Width; x++) {
                // Check row
                var xFits = x + inaRow <= Width;
                if (xFits) {
                    for (var i = 0; i < inaRow; i++)
                        winner[i] = new(x + i, y);
                    if (IsWinningGroup(grid, winner)) return true;

                    if (yFits) {
                        // Check diagonals
                        for (var i = 0; i < inaRow; i++)
                            winner[i] = new(x + i, y + i);
                        if (IsWinningGroup(grid, winner)) return true;
                    }
                }
                if (yFits) {
                    // Check column
                    for (var i = 0; i < inaRow; i++)
                        winner[i] = new(x, y + i);
                    if (IsWinningGroup(grid, winner)) return true;
                }
            }
        }

        winner = null;
        return false;
    }

    private static bool IsWinningGroup(bool?[,] grid, ReadOnlySpan<Coordinate<int>> group) {
        if (group is not [var firstItem, .. var rest]) return false;
        var first = grid[firstItem.X, firstItem.Y];
        if (first is null) return false;
        foreach (var other in rest)
            if (grid[other.X, other.Y] != first)
                return false;
        return true;
    }
    #endregion
}
