namespace AkaiApcMini.Games;

public interface IGameState<TSelf> where TSelf : IGameState<TSelf> {
    static abstract TSelf Create();
    void Reset();
}
