using AkaiApcMini.Controls;
using AkaiApcMini.Display;
using static AkaiApcMini.Games.GameMenu;

namespace AkaiApcMini.Games;
public class GameMenu : SyncGameBase<MenuState, ButtonColor> {
    public GameMenu(ILogger<GameMenu> logger, IEnumerable<ISyncGame<ButtonColor>> games) : base(logger) {
        this.logger = logger;
        Games = games.ToArray();
        RegisterEventListeners();
    }

    private void RegisterEventListeners() {
        foreach (var game in Games)
            game.UpdateAvailable += Game_UpdateAvailable;
    }

    private void Game_UpdateAvailable(ISyncGame<ButtonColor> sender) {
        // Only update game if the sending game is active
        if (ReferenceEquals(sender, SelectedGame))
            RequestAnimation();
    }

    private readonly ISyncGame<ButtonColor>[] Games;
    private readonly object gameChangeLock = new();
    private readonly ILogger<GameMenu> logger;

    private ISyncGame<ButtonColor>? SelectedGame {
        get => State.SelectedGame; set {
            lock (gameChangeLock) {
                var selectedGame = State.SelectedGame;
                if (ReferenceEquals(selectedGame, value)) return;
                if (selectedGame is not null) ExitGame(selectedGame);
                if (value is not null) StartGame(value);
                State.SelectedGame = value;
            }
        }
    }

    protected override void RunningStateChanged(bool running) {
        if (SelectedGame is { } game) {
            if (running) game.Start();
            else game.Stop();
        }
    }

    public override void Draw(Graphics<ButtonColor> display) {
        if (SelectedGame is { } game) game.Draw(display);
        else DrawMenu(display);
    }

    private void DrawMenu(Graphics<ButtonColor> graphics)
        // Draw background (thumbnail)
        => Games[State.GameIndex].Draw(graphics);

    public class MenuState : IGameState<MenuState> {
        public int GameIndex;
        public ISyncGame<ButtonColor>? SelectedGame;

        public static MenuState Create() => new();

        public void Reset() {
            GameIndex = 0;
            SelectedGame = null;
        }
    }

    public override void ButtonDown(int channel, int button) {
        const int EscapeButton = 98;
        if (State.SelectedGame is { } game) {
            if (button is not EscapeButton) {
                game.ButtonDown(channel, button);
                return;
            } else SelectedGame = null;
        } else {
            if (button is 66) State.GameIndex = (State.GameIndex + (Games.Length - 1)) % Games.Length;
            else if (button is 67) State.GameIndex = (State.GameIndex + 1) % Games.Length;
            else if (button is EscapeButton) SelectedGame = Games[State.GameIndex];
        }
        RequestAnimation();
    }

    private void ExitGame(ISyncGame<ButtonColor> game) {
        logger.LogInformation("Game {name} left to menu!", game.GetType().Name);
        game.Stop();
        game.Reset();
    }

    private void StartGame(ISyncGame<ButtonColor> game) {
        logger.LogInformation("Game {name} was started!", game.GetType().Name);
        game.Start();
    }

    public override void FaderInput(int channel, int id, int value)
        => SelectedGame?.FaderInput(channel, id, value);
}
