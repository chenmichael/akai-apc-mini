using System.Numerics;
using AkaiApcMini.Controls;
using AkaiApcMini.Display;
using static AkaiApcMini.Games.PongGame.PongState;

namespace AkaiApcMini.Games;
public class PongGame(ILogger<PongGame> logger) : AsyncGameBase<PongGame.PongState, ButtonColor>(logger, 50) {
    public class PongState : IGameState<PongState> {
        public static readonly Coordinate<float> InitialPosition = new(1, Height / 2);
        public static readonly Coordinate<float> InitialDirection = new Coordinate<float>(1, 0) * BallSpeed;
        private const float InitialHeight = (Height - 1) / 2.0f;

        public Coordinate<float> BallPosition;
        public Coordinate<float> BallDirection;
        public GameState State;
        public int TimeSinceDeath;
        public float LeftPaddle;
        public float RightPaddle;

        public enum GameState {
            Running,
            Dead,
        }

        public void Reset() {
            BallPosition = InitialPosition;
            BallDirection = InitialDirection;
            State = GameState.Running;
            TimeSinceDeath = 0;
            LeftPaddle = InitialHeight;
            RightPaddle = InitialHeight;
        }

        public static PongState Create() => new();
    }

    // Game config
    public const int Width = 8;
    public const int Height = 8;
    public const float HalfPaddleHeight = 3.0f / 2;
    private const float BallSpeed = 1.0f / 6.0f;
    private const float FloatPi = (float)Math.PI;

    public static Coordinate<int> Rounded(Coordinate<float> coordinate)
        => new((int)Math.Round(coordinate.X), (int)Math.Round(coordinate.Y));

    public override void Draw(Graphics<ButtonColor> graphics) {
        // Clear Screen
        graphics.Fill(ButtonColor.Off);

        var ballGridPos = Rounded(State.BallPosition);

        // Draw blood
        if (State.State is GameState.Dead) {
            for (var x = 0; x < Width; x++)
                for (var y = 0; y < Height; y++) {
                    var dx = x - ballGridPos.X;
                    var dy = y - ballGridPos.Y;
                    var distance = (int)Math.Sqrt((dx * dx) + (dy * dy));
                    if (distance < State.TimeSinceDeath)
                        graphics.SetColor(x, y, ButtonColor.Red);
                }
        }

        // Draw Snake
        DrawPaddle(graphics, State.LeftPaddle, 0);
        DrawPaddle(graphics, State.RightPaddle, Width - 1);

        // Draw Ball
        graphics.SetColor(ballGridPos, ButtonColor.Red);
    }

    private static void DrawPaddle(Graphics<ButtonColor> graphics, float height, int column) {
        var bottom = (int)Math.Ceiling(height - HalfPaddleHeight);
        var top = (int)Math.Floor(height + HalfPaddleHeight);
        for (var y = Math.Max(0, bottom); y <= Math.Min(top, Height - 1); y++)
            graphics.SetColor(column, y, ButtonColor.Green);
    }

    protected override void UpdateState() {
        switch (State.State) {
        case GameState.Running:
            AdvanceBall();
            break;
        case GameState.Dead:
            if (++State.TimeSinceDeath > 100) Reset();
            break;
        default: throw new NotImplementedException($"Game state {State}");
        }
    }

    private void AdvanceBall() {
        var dPos = State.BallDirection;
        var newPos = State.BallPosition + dPos;
        if (newPos.X - (Width - 1) is > 0 and var xOvershoot) {
            newPos = ReflectPaddle(xOvershoot, dPos, true);
        } else if (newPos.X is < 0 and var xUndershoot) {
            newPos = ReflectPaddle(xUndershoot, dPos, false);
        }

        State.BallPosition = ReflectTopBottom(State.BallPosition, newPos);
    }

    private const float AngleDiff = 30.0f * FloatPi / 180.0f;
    private const float FaderMultiplier = (float)(Height - 1) / sbyte.MaxValue;

    private Coordinate<float> ReflectPaddle(float overshoot, Coordinate<float> dPos, bool right) {
        var overshootRatio = overshoot / dPos.X;
        var completedRatio = 1 - overshootRatio;
        var collisionPoint = State.BallPosition + (dPos * completedRatio);
        collisionPoint = ReflectTopBottom(State.BallPosition, collisionPoint);

        var paddleHeight = right ? State.RightPaddle : State.LeftPaddle;
        var paddleOffset = collisionPoint.Y - paddleHeight;
        if (Math.Abs(paddleOffset) > HalfPaddleHeight) {
            State.State = GameState.Dead;
            return collisionPoint;
        }

        var ratio = Ratio(-HalfPaddleHeight, HalfPaddleHeight, paddleOffset);
        var newAngle = right
            ? Map(FloatPi + AngleDiff, FloatPi - AngleDiff, ratio)
            : Map(-AngleDiff, AngleDiff, ratio);
        State.BallDirection = AngleToDirection(newAngle) * BallSpeed;
        return collisionPoint + (State.BallDirection * overshootRatio);
    }

    private Coordinate<float> ReflectTopBottom(Coordinate<float> prevPos, Coordinate<float> newPos) {
        var dPos = newPos - prevPos;
        if (newPos.Y - (Height - 1) is > 0 and var yOvershoot)
            return InvertYDirection(prevPos, dPos, yOvershoot);
        else if (newPos.Y is < 0 and var yUndershoot)
            return InvertYDirection(prevPos, dPos, yUndershoot);
        return newPos;
    }

    private Coordinate<float> InvertYDirection(Coordinate<float> prevPos, Coordinate<float> dPos, float yUndershoot) {
        var undershootRatio = yUndershoot / dPos.Y;
        var completedRatio = 1 - undershootRatio;
        var collisionPoint = prevPos + (dPos * completedRatio);

        State.BallDirection.Y = -State.BallDirection.Y;
        return collisionPoint + (State.BallDirection * undershootRatio);
    }

    private static Coordinate<float> AngleToDirection(float angle)
        => new((float)Math.Cos(angle), (float)Math.Sin(angle));

    public static TTarget Map<TSource, TTarget>(TSource fromMin, TSource fromMax, TTarget toMin, TTarget toMax, TSource val)
        where TSource : INumber<TSource>
        where TTarget : INumber<TTarget> {
        var ratio = Ratio(fromMin, fromMax, val);
        return Map(toMin, toMax, TTarget.CreateChecked(ratio));
    }

    private static T Map<T>(T toMin, T toMax, T ratio)
        where T : INumber<T>
        => toMin + ((toMax - toMin) * ratio);

    private static T Ratio<T>(T min, T max, T val)
        where T : INumber<T>
        => (val - min) / (max - min);

    public override void FaderInput(int channel, int id, int value) {
        switch (id) {
        case 48: State.LeftPaddle = value * FaderMultiplier; break;
        case 55: State.RightPaddle = value * FaderMultiplier; break;
        }
    }
}
