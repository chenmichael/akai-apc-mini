namespace AkaiApcMini.Games;

public class GameRunnerEnvironment<TColor> : IDisposable {
    private readonly ISyncGame<TColor> game;
    private readonly IDisposable connector;

    public GameRunnerEnvironment(ISyncGame<TColor> game, IDisposable connector) {
        game.Start();
        this.game = game;
        this.connector = connector;
    }

    private void Stop() {
        // disconnect event connector before stopping to prevent unhandled events
        connector.Dispose();
        game.Stop();
    }

    #region Disposing
    private bool isDisposed;

    protected virtual void Dispose(bool disposing) {
        if (isDisposed) return;
        if (disposing)
            Stop();

        isDisposed = true;
    }

    public void Dispose() {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
