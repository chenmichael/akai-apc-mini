using AkaiApcMini.Controls;
using AkaiApcMini.Display;
using static AkaiApcMini.Games.SnakeGame.GameState;

namespace AkaiApcMini.Games;
public class SnakeGame(ILogger<SnakeGame> logger) : AsyncGameBase<SnakeGame.GameState, ButtonColor>(logger, 4) {
    // Game config
    public const int Width = 8;
    public const int Height = 8;
    public static readonly Coordinate<int> InitialPosition = new(Width / 2, Height / 2);

    // State
    public class GameState : IGameState<GameState> {
        public Direction LastDirection;
        public Direction? NewDirection = null;
        public Coordinate<int> ApplePosition;
        public int Score = 0;
        public GameStatus State;
        public int TimeSinceDeath;
        public LinkedList<Coordinate<int>> Snake = new();

        public enum GameStatus {
            WaitForInput,
            Running,
            Dead,
        }

        public void Reset() {
            Snake.Clear();
            _ = Snake.AddFirst(InitialPosition);
            NewDirection = null;
            Score = 0;
            TimeSinceDeath = 0;
            NewApplePosition();
            State = GameStatus.WaitForInput;
        }

        private Coordinate<int> RandomFreeSpot() {
            var usedPositions = new HashSet<Coordinate<int>>(Snake);
            Span<Coordinate<int>> freeFields = stackalloc Coordinate<int>[Width * Height];
            var freeFieldCount = 0;
            var pos = Coordinate<int>.Zero;
            for (pos.X = 0; pos.X < Width; pos.X++)
                for (pos.Y = 0; pos.Y < Height; pos.Y++)
                    if (usedPositions.Contains(pos)) continue;
                    else freeFields[freeFieldCount++] = pos;
            return freeFields[Random.Shared.Next(0, freeFieldCount)];
        }

        public void NewApplePosition() => ApplePosition = RandomFreeSpot();

        public static GameState Create() => new();
    }

    public enum Direction {
        Up, Right, Down, Left,
    }

    public void ChangeDirection(Direction? optDirection) {
        if (optDirection is not { } direction) return;
        if (State.NewDirection is null && State.State is GameStatus.WaitForInput) {
            State.NewDirection = direction;
            State.State = GameStatus.Running;
            logger.LogInformation("Game was started in direction {dir}!", direction);
        } else if (State.LastDirection != OppositeDirection(direction)) {
            State.NewDirection = direction;
            logger.LogInformation("Direction was changed to {dir}!", direction);
        } else
            logger.LogWarning("Cannot go into opposite direction {dir}, from previous {prevDir}!", direction, State.LastDirection);
    }

    protected override void UpdateState() {
        switch (State.State) {
        case GameStatus.Running when State.NewDirection is { } newDir:
            AdvanceSnake(State.LastDirection = newDir);
            break;
        case GameStatus.WaitForInput: break;
        case GameStatus.Dead:
            if (++State.TimeSinceDeath > 20) Reset();
            break;
        default: throw new NotImplementedException($"Game state {State}");
        }
    }

    private void AdvanceSnake(Direction direction) {
        var head = (State.Snake.First ?? throw new InvalidProgramException("Snake is headless!")).Value;
        var newHead = head.Advance(direction);
        newHead.X = (newHead.X + Width) % Width;
        newHead.Y = (newHead.Y + Height) % Height;

        // Check game over
        if (State.Snake.Contains(newHead)) {
            logger.LogWarning("Snake crashed at {pos}!", newHead);
            State.State = GameStatus.Dead;
        } else _ = State.Snake.AddFirst(newHead);

        // Check apple eaten
        if (State.ApplePosition == newHead) {
            State.Score++;
            logger.LogInformation("Apple at {pos} was eaten, score is now {score}!", State.ApplePosition, State.Score);
            State.NewApplePosition();
        } else State.Snake.RemoveLast();
    }

    private static Direction OppositeDirection(Direction direction) => (Direction)(((int)direction + 2) % 4);

    public override void Draw(Graphics<ButtonColor> graphics) {
        // Clear Screen
        graphics.Fill(ButtonColor.Off);

        var head = (State.Snake.First ?? throw new InvalidProgramException("Snake is headless!")).Value;

        if (State.State is GameStatus.Dead) {
            // Draw blood
            for (var x = 0; x < Width; x++)
                for (var y = 0; y < Height; y++) {
                    var dx = x - head.X;
                    var dy = y - head.Y;
                    var distance = (int)Math.Sqrt((dx * dx) + (dy * dy));
                    if (distance < State.TimeSinceDeath)
                        graphics.SetColor(x, y, ButtonColor.Red);
                }
        }

        // Draw Apple
        if (State.State is GameStatus.Running)
            graphics.SetColor(State.ApplePosition, ButtonColor.Red);

        // Draw Snake
        DrawSnake(graphics);

        // Draw direction hints
        if (State.State is GameStatus.WaitForInput)
            foreach (var dir in Enum.GetValues<Direction>())
                graphics.SetColor(head.Advance(dir), ButtonColor.YellowFlashing);

        if (State.State is GameStatus.Dead)
            // Draw score
            graphics.DrawNumber(State.Score, ButtonColor.GreenFlashing);
    }

    private void DrawSnake(Graphics<ButtonColor> graphics) {
        var color = ButtonColor.Yellow;
        foreach (var pos in State.Snake)
            graphics.SetColor(pos, State.State is GameStatus.Dead
                ? ButtonColor.RedFlashing
                : (color = ToggleColor(color)));

        static ButtonColor ToggleColor(ButtonColor color) => color == ButtonColor.Yellow ? ButtonColor.Green : ButtonColor.Yellow;
    }

    public override void ButtonDown(int channel, int button)
        => ChangeDirection(button switch {
            64 => Direction.Up,
            65 => Direction.Down,
            66 => Direction.Left,
            67 => Direction.Right,
            _ => null,
        });
}
