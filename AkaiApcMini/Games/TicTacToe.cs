using AkaiApcMini.Controls;
using AkaiApcMini.Display;

namespace AkaiApcMini.Games;

public class TicTacToe(ILogger<TicTacToe> logger) : MnkGame<TicTacToe.Dimensions, ButtonColor>(logger, SIZE) {
    public const int SIZE = 3;
    public class Dimensions : IFixedDimensions {
        public static int Width => SIZE;
        public static int Height => SIZE;
    }

    #region Drawing
    public override void Draw(Graphics<ButtonColor> graphics) {
        graphics.Fill(ButtonColor.Off);
        DrawGrid(graphics, State.Grid, ButtonColor.Yellow);

        // Draw winner
        if (State.WinningGroup is { } winner)
            foreach (var field in winner) {
                var player = State.Grid[field.X, field.Y] ?? throw new InvalidProgramException("Winning group contains invalid field!");
                DrawField(graphics, field.X, field.Y, player, true);
            }

        // Indicate active player if game is active
        if (State.WinningGroup is null)
            DrawActivePlayer(graphics, State.ActivePlayer);
    }

    private static void DrawActivePlayer(Graphics<ButtonColor> graphics, bool player) {
        var color = PlayerColor(player, true);
        graphics.SetColor(2, 2, color);
        graphics.SetColor(2, 5, color);
        graphics.SetColor(5, 2, color);
        graphics.SetColor(5, 5, color);
    }

    private static void DrawGrid(Graphics<ButtonColor> graphics, bool?[,] grid, ButtonColor color) {
        graphics.DrawHorizontalLine(2, 0, 8, color);
        graphics.DrawHorizontalLine(5, 0, 8, color);
        graphics.DrawVerticalLine(2, 0, 8, color);
        graphics.DrawVerticalLine(5, 0, 8, color);

        for (var y = 0; y < SIZE; y++)
            for (var x = 0; x < SIZE; x++)
                if (grid[x, y] is { } player)
                    DrawField(graphics, x, y, player);
    }

    private static void DrawField(Graphics<ButtonColor> graphics, int x, int y, bool player, bool winning = false) {
        var gridX = 3 * x;
        var gridY = 3 * y;
        graphics.DrawFilledRectangle(gridX, gridY, gridX + 2, gridY + 2, PlayerColor(player, winning));
    }

    private static ButtonColor PlayerColor(bool player, bool winning = false) => winning
        ? (player ? ButtonColor.RedFlashing : ButtonColor.GreenFlashing)
        : (player ? ButtonColor.Red : ButtonColor.Green);

    private static string PlayerName(bool player)
        => PlayerColor(player).ToString();
    #endregion

    protected override void FieldSelected(int x, int y) {
        if (x % 3 is 2) return; // Ignore clicks on grid
        var gridX = x / 3;
        if (y % 3 is 2) return; // Ignore clicks on grid
        var gridY = y / 3;
        ClickField(gridX, gridY);
    }

    private void ClickField(int x, int y) {
        ref var field = ref State.Grid[x, y];
        if (field is { } occupant) {
            logger.LogWarning("Player {player} clicked on field ({x}, {y}) already occupied by {occupant}!", PlayerName(State.ActivePlayer), x, y, PlayerName(occupant));
            return; // Ignore clicks on occupied field
        }

        logger.LogInformation("Player {player} clicked on field ({x}, {y})!", PlayerName(State.ActivePlayer), x, y);

        field = State.ActivePlayer;
        State.ActivePlayer = !State.ActivePlayer;
    }
}
