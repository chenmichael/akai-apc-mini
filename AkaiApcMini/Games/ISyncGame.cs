using AkaiApcMini.Controls;
using AkaiApcMini.Display;

namespace AkaiApcMini.Games;

/// <summary>
/// Provides an interface for games that run synchronously with user input.
/// </summary>
public interface ISyncGame<TColor> : IInputHandler {
    /// <summary>
    /// Waits for a new animation frame.
    /// </summary>
    /// <param name="token">Cancellation token to observe</param>
    Task WaitForFrameAsync(CancellationToken token);
    /// <summary>
    /// Event that is raised once an update becomes available.
    /// </summary>
    event UpdateHandler<TColor> UpdateAvailable;
    /// <summary>
    /// Draws an awaited animation frame to the screen.
    /// </summary>
    /// <param name="display">Graphics display to draw onto.</param>
    void Draw(Graphics<TColor> display);
    /// <summary>
    /// Start the game.
    /// </summary>
    void Start();
    /// <summary>
    /// Pause the game.
    /// </summary>
    void Stop();
    /// <summary>
    /// Resets the game state.
    /// </summary>
    void Reset();
}

public delegate void UpdateHandler<TColor>(ISyncGame<TColor> sender);
