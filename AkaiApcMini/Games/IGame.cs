using AkaiApcMini.Controls;
using AkaiApcMini.Display;

namespace AkaiApcMini.Games;

public interface IGame {
    int FrameRate { get; }
    void Update();
    void Draw(Graphics<ButtonColor> graphics);
    void InvokeButtonDown(int channel, int id);
    void InvokeFaderInput(int channel, int id, int value);
    void Reset();
}
