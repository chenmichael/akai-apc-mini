namespace AkaiApcMini.Games;

public class CancellableTask {
    private readonly CancellationTokenSource cts = new();
    private readonly Task task;
    public CancellableTask(Func<CancellationToken, Task> loop) => task = Task.Run(() => loop(cts.Token));
    public void Cancel() => cts.Cancel();
    public async Task CancelAsync() => await cts.CancelAsync();
    public void Wait() => task.Wait();
    public async Task WaitAsync(CancellationToken ct) => await task.WaitAsync(ct);
}
