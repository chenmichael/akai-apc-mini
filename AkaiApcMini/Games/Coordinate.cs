using System.Numerics;
using static AkaiApcMini.Games.SnakeGame;

namespace AkaiApcMini.Games;

public record struct Coordinate<T>(T X, T Y) where T : INumber<T> {
    public static readonly Coordinate<T> Zero = new(T.Zero, T.Zero);

    public Coordinate<T> Advance(Direction direction) => direction switch {
        Direction.Up => this with { Y = Y + T.One },
        Direction.Right => this with { X = X + T.One },
        Direction.Down => this with { Y = Y - T.One },
        Direction.Left => this with { X = X - T.One },
        _ => throw new ArgumentOutOfRangeException(nameof(direction), $"Unknown direction '{direction}'!"),
    };

    public static Coordinate<T> operator /(Coordinate<T> coordinate, T div)
        => new(coordinate.X / div, coordinate.Y / div);

    public static Coordinate<T> operator *(Coordinate<T> coordinate, T mul)
        => new(coordinate.X * mul, coordinate.Y * mul);

    public static Coordinate<T> operator +(Coordinate<T> lhs, Coordinate<T> rhs)
        => new(lhs.X + rhs.X, lhs.Y + rhs.Y);

    public static Coordinate<T> operator -(Coordinate<T> lhs, Coordinate<T> rhs)
        => new(lhs.X - rhs.X, lhs.Y - rhs.Y);

    public static Coordinate<T> operator -(Coordinate<T> lhs)
        => new(-lhs.X, -lhs.Y);

    public readonly Coordinate<TTarget> ConvertTo<TTarget>() where TTarget : INumber<TTarget>
        => new(TTarget.CreateChecked(X), TTarget.CreateChecked(Y));
}
