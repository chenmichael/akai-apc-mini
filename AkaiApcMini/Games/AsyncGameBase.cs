namespace AkaiApcMini.Games;

public abstract class AsyncGameBase<TState, TColor>(ILogger<AsyncGameBase<TState, TColor>> logger, TimeSpan period) : SyncGameBase<TState, TColor>(logger)
    where TState : IGameState<TState> {
    public AsyncGameBase(ILogger<AsyncGameBase<TState, TColor>> logger, int frameRate) : this(logger, TimeSpan.FromMilliseconds(1000.0d / frameRate)) { }
    private readonly PeriodicTimer timer = new(period);
    private readonly object taskLock = new();
    private CancellableTask? gameTask;

    private async Task GameLoop(CancellationToken ct) {
        if (!ct.CanBeCanceled) throw new ArgumentException("Game loop cannot be cancelled!", nameof(ct));
        logger.LogInformation("Game loop is started!");
        while (true) {
            try {
                _ = await timer.WaitForNextTickAsync(ct);
            } catch (OperationCanceledException) { break; }
            if (Running) {
                logger.LogDebug("Ticking game state!");
                UpdateState();
                RequestAnimation();
            } else {
                logger.LogWarning("Game loop is running, even though the game is not in running state!");
                break;
            }
        }
        _ = CloseGameTask();
        logger.LogInformation("Game loop has stopped!");
    }

    private CancellableTask? CloseGameTask() {
        lock (taskLock)
            if (gameTask is { } task) {
                task.Cancel();
                gameTask = null;
                return task;
            }
        return null;
    }

    protected override void RunningStateChanged(bool running) {
        lock (taskLock) {
            if (running) {
                if (CloseGameTask() is { } task)
                    WaitSafe(task);
                gameTask = gameTask is null ?
                    new(GameLoop)
                    : throw new InvalidProgramException("Game task was already running!");
            } else {
                if (gameTask is { } task) task.Cancel();
            }
        }
    }

    private static void WaitSafe(CancellableTask task) {
        try {
            task.Wait();
        } catch (Exception) { }
    }

    protected abstract void UpdateState();
}
