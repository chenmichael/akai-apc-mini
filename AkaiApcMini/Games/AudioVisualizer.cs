using System.Collections.Immutable;
using System.Diagnostics;
using AkaiApcMini.Controls;
using AkaiApcMini.Display;
using FftSharp;
using NAudio.Wave;

namespace AkaiApcMini.Games;

public class AudioVisualizer : SyncGameBase<AudioVisualizer.Data, ButtonColor> {
    public AudioVisualizer(ILogger<AudioVisualizer> logger) : base(logger) {
        WaveInput = GetAudioDevice(logger);
        WaveInput.DataAvailable += WaveInput_DataAvailable;
    }

    protected override void RunningStateChanged(bool running) {
        if (running) WaveInput.StartRecording();
        else WaveInput.StopRecording();
    }

    public const int ROWS = 8;
    public const int COLUMNS = 8;
    private const int BufferMilliseconds = 20;
    private const int SampleRate = 44100;
    private const int MaxValue = 200;
    private const double MaxFader = sbyte.MaxValue - 1;
    private const int BytesPerValue = sizeof(short);
    private const int BitDepth = 8 * BytesPerValue;

    #region Filtering
    private const double Suppression = 0.95;
    private const double SuppressionWideness = 0.25;
    public static IReadOnlyList<double> SuppressionFilter { get; } = InitializeSuppressionFilter();
    private static IReadOnlyList<double> InitializeSuppressionFilter() {
        var filter = new double[COLUMNS];
        for (var i = 0; i < filter.Length; i++)
            filter[i] = 1 - (Suppression * Math.Exp(-SuppressionWideness * i));
        return filter.ToImmutableArray();
    }
    #endregion

    #region Audio Device
    private readonly WaveInEvent WaveInput;
    private static WaveInEvent GetAudioDevice(ILogger<AudioVisualizer> logger) {
        var deviceNumber = SelectAudioDevice(logger);
        return new WaveInEvent {
            BufferMilliseconds = BufferMilliseconds,
            DeviceNumber = deviceNumber,
            WaveFormat = new WaveFormat(rate: SampleRate, bits: BitDepth, channels: 1)
        };
    }
    private static int SelectAudioDevice(ILogger<AudioVisualizer> logger) {
        if (WaveInEvent.DeviceCount is not (not 0 and var count))
            throw new InvalidProgramException("No audio devices are available!");
        for (var i = 0; i < count; i++) {
            var caps = WaveInEvent.GetCapabilities(i);
            logger.LogInformation("Found wave in device {number}: {device}", i, caps.ProductName);
        }
        return 0;
    }
    #endregion

    public class Data : IGameState<Data> {
        private const double MaxGain = 1.0d;

        public static Data Create() => new();

        public double[] Levels = new double[COLUMNS];
        public double MasterGain;
        public double[] WaveBuffer = new double[SampleRate * BufferMilliseconds / 1000];
        public double[] Gain = new double[COLUMNS];

        public void Reset() {
            MasterGain = MaxGain;
            Array.Fill(Gain, MaxGain);
        }
    }

    public override void Draw(Graphics<ButtonColor> display) {
        display.Fill(ButtonColor.Off);

        for (var x = 0; x < COLUMNS; x++) {
            var level = (int)Math.Round(State.Levels[x] * ROWS * State.MasterGain * State.Gain[x]);
            for (var y = 0; y < ROWS; y++) {
                display.SetColor(x, y, y switch {
                    _ when level == y => ButtonColor.Red,
                    _ when level - y is > 0 and <= 2 => ButtonColor.Yellow,
                    _ when level - y is > 2 => ButtonColor.Green,
                    _ => ButtonColor.Off,
                });
            }
        }
    }

    private void WaveInput_DataAvailable(object? sender, WaveInEventArgs e) {
        // Zero padded buffer
        var values = e.Buffer.Length / BytesPerValue;
        var bufferSize = RoundToPowerOf2(values);
        Debug.Assert(values <= bufferSize);

        Span<System.Numerics.Complex> buffer = stackalloc System.Numerics.Complex[bufferSize];
        buffer.Clear();
        // Read buffer
        for (var i = 0; i < values; i++)
            buffer[i] = BitConverter.ToInt16(e.Buffer, i * BytesPerValue);

        // calculate fft
        FFT.Forward(buffer);
        Span<double> fftBuffer = stackalloc double[(bufferSize / 2) + 1];
        var fft = Magnitude(fftBuffer, buffer);

        // ignore upper half of frequencies
        Span<double> levels = State.Levels;

        // Calculate mel scale
        Span<double> mel = stackalloc double[2 * levels.Length];
        Scale(fft, mel, SampleRate);

        // Apply suppression filter
        for (var i = 0; i < levels.Length; i++)
            levels[i] = mel[i] * SuppressionFilter[i] / MaxValue;

        RequestAnimation();
    }

    #region FFT Stuff
    /// <summary>
    /// Return frequencies for each index of a FFT.
    /// </summary>
    public static void FrequencyScale(Span<double> frequencies, double sampleRate) {
        var fftPeriodHz = sampleRate / (frequencies.Length - 1) / 2;
        for (var i = 0; i < frequencies.Length; i++)
            frequencies[i] = i * fftPeriodHz;
    }

    /// <summary>
    /// Calculate power spectrum density (PSD) in RMS units
    /// </summary>
    public static Span<double> Magnitude(Span<double> buffer, ReadOnlySpan<System.Numerics.Complex> spectrum) {
        var length = (spectrum.Length / 2) + 1;
        ArgumentOutOfRangeException.ThrowIfLessThan(buffer.Length, length);
        var output = buffer[..length];
        // first point (DC component) is not doubled
        output[0] = spectrum[0].Magnitude / spectrum.Length;
        // subsequent points are doubled to account for combined positive and negative frequencies
        for (var i = 1; i < output.Length; i++)
            output[i] = 2 * spectrum[i].Magnitude / spectrum.Length;
        return output;
    }

    public static void Scale(ReadOnlySpan<double> fft, Span<double> fftMel, int sampleRate) {
        double freqMax = sampleRate / 2;
        var maxMel = Mel.FromFreq(freqMax);

        var melBinCount = fftMel.Length;
        var melPerBin = maxMel / (melBinCount + 1);
        for (var binIndex = 0; binIndex < melBinCount; binIndex++) {
            var melLow = melPerBin * binIndex;
            var melHigh = melPerBin * (binIndex + 2);

            var freqLow = Mel.ToFreq(melLow);
            var freqHigh = Mel.ToFreq(melHigh);

            var indexLow = (int)(fft.Length * freqLow / freqMax);
            var indexHigh = (int)(fft.Length * freqHigh / freqMax);
            var indexSpan = indexHigh - indexLow;

            double binScaleSum = 0;
            for (var i = 0; i < indexSpan; i++) {
                var binFrac = (double)i / indexSpan;
                var indexScale = (binFrac < .5) ? binFrac * 2 : 1 - binFrac;
                binScaleSum += indexScale;
                fftMel[binIndex] += fft[indexLow + i] * indexScale;
            }
            fftMel[binIndex] /= binScaleSum;
        }
    }

    private static int RoundToPowerOf2(int value) {
        ArgumentOutOfRangeException.ThrowIfNegative(value);
        var i = 1;
        do {
            if (i >= value) return i;
        } while ((i <<= 1) > 0);
        throw new UnreachableException($"No power of two larger than {value} found!");
    }
    #endregion

    public override void FaderInput(int channel, int id, int value) {
        var fader = id - 48;
        var ratio = value / MaxFader;
        if (fader is 8)
            State.MasterGain = ratio;
        else if (fader is >= 0 and < 8)
            State.Gain[fader] = ratio;
    }
}
