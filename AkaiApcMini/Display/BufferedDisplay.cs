namespace AkaiApcMini.Display;

public class BufferedDisplay<TColor>(ILogger<BufferedDisplay<TColor>>? logger, IDisplay<TColor> backend) : IFlushableDisplay<TColor> {
    private TColor[,] buffer = new TColor[backend.Width, backend.Height];
    private TColor[,] oldBuffer = new TColor[backend.Width, backend.Height];
    private bool neverFlushed = true;

    public int Width => backend.Width;
    public int Height => backend.Height;
    public void SetColor(int x, int y, TColor color) => buffer[x, y] = color;

    public void Flush() {
        var flushCount = 0;
        for (var y = 0; y < backend.Height; y++)
            for (var x = 0; x < backend.Width; x++) {
                var pixel = buffer[x, y];
                if (neverFlushed || !EqualityComparer<TColor>.Default.Equals(pixel, oldBuffer[x, y])) {
                    backend.SetColor(x, y, pixel);
                    flushCount++;
                }
            }
        // Swap front and back buffer
        // Back buffer now contains current pixels
        // Front buffer is now invalidated (must be re-drawn fully)
        (buffer, oldBuffer) = (oldBuffer, buffer);
        neverFlushed = false;
        logger?.LogDebug("{pixels} pixels were flushed!", flushCount);
    }
}
