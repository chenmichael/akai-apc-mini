namespace AkaiApcMini.Display;

public interface IDisplay<TColor> {
    int Width { get; }
    int Height { get; }
    void SetColor(int x, int y, TColor color);
}
