namespace AkaiApcMini.Display;

public interface IFlushableDisplay<TColor> : IDisplay<TColor> {
    void Flush();
}
