using AkaiApcMini.Games;

namespace AkaiApcMini.Display;

/// <summary>
/// Provides a graphics class for drawing on display.
/// Should be used in combination with the <see cref="BufferedDisplay{TColor}"/> to reduce the raw draw calls to the <paramref name="display"/> if it is performance critical.
/// </summary>
/// <typeparam name="TColor">Color type of the underlying display.</typeparam>
/// <param name="logger">Logger used to log useful information.</param>
/// <param name="display">Backing display to draw on.</param>
public class Graphics<TColor>(ILogger<Graphics<TColor>> logger, IDisplay<TColor> display) : IFlushableDisplay<TColor> {
    public int Width => display.Width;
    public int Height => display.Height;
    public void SetColor(int x, int y, TColor color) => display.SetColor(x, y, color);
    public void SetColor(Coordinate<int> coordinate, TColor color) => SetColor(coordinate.X, coordinate.Y, color);
    /// <summary>
    /// Flush the underlying display if it needs to be flushed.
    /// It is considered a display that needs to be flushed if it is an <see cref="IFlushableDisplay{TColor}"/>.
    /// Emits a warning if the display does not need flushing.
    /// </summary>
    public void Flush() {
        if (display is not IFlushableDisplay<TColor> flushable) {
            logger.LogWarning("Backing display of type {display} does not need to be flushed!", display.GetType().Name);
            return;
        }
        flushable.Flush();
    }
    public void Fill(TColor color) {
        for (var y = 0; y < display.Height; y++)
            for (var x = 0; x < display.Width; x++)
                display.SetColor(x, y, color);
    }
    public void DrawLine(int xStart, int yStart, int xEnd, int yEnd, TColor color) {
        if (xStart == xEnd) DrawVerticalLine(xStart, yStart, yEnd, color);
        var dx = xEnd - xStart;
        var dy = yEnd - yStart;
        var D = (2 * dy) - dx;
        var y = yStart;

        for (var x = xStart; x < xEnd; x++) {
            SetColor(x, y, color);
            if (D > 0) {
                y++;
                D -= 2 * dx;
            }
            D += 2 * dy;
        }
    }
    public void DrawVerticalLine(int x, int yStart, int yEnd, TColor color) {
        for (var y = yStart; y < yEnd; y++)
            SetColor(x, y, color);
    }
    public void DrawHorizontalLine(int y, int xStart, int xEnd, TColor color) {
        for (var x = xStart; x < xEnd; x++)
            SetColor(x, y, color);
    }
    public void DrawFilledRectangle(int xStart, int yStart, int xEnd, int yEnd, TColor color) {
        for (var y = yStart; y < yEnd; y++)
            for (var x = xStart; x < xEnd; x++)
                SetColor(x, y, color);
    }
    public void DrawRectangle(int xStart, int yStart, int xEnd, int yEnd, TColor border, TColor? fill) {
        DrawHollowRectangle(xStart, yStart, xEnd, yEnd, border);
        if (fill is { }) DrawFilledRectangle(xStart + 1, yStart + 1, xEnd - 1, yEnd - 1, fill);
    }
    public void DrawHollowRectangle(int xStart, int yStart, int xEnd, int yEnd, TColor color) {
        DrawVerticalLine(xStart, yStart, yEnd, color);
        DrawVerticalLine(xEnd - 1, yStart, yEnd, color);
        DrawHorizontalLine(yStart, xStart, xEnd, color);
        DrawHorizontalLine(yEnd - 1, xStart, xEnd, color);
    }
    public void DrawDigit(int digit, int xStart, TColor color) {
        const int width = 3;
        const int endCol = width - 1;
        switch (digit) {
        case 0:
            DrawHollowRectangle(xStart, 0, xStart + width, Height, color);
            break;
        case 1:
            DrawVerticalLine(xStart + endCol, 0, Height, color);
            SetColor(xStart, Height - 3, color);
            SetColor(xStart + 1, Height - 2, color);
            break;
        case 2:
            DrawHorizontalLine(0, xStart, xStart + width, color);
            DrawVerticalLine(xStart, 5, Height - 1, color);
            DrawVerticalLine(xStart, 0, 3, color);
            SetColor(xStart + 1, 3, color);
            DrawVerticalLine(xStart + endCol, 4, Height - 1, color);
            SetColor(xStart + 1, Height - 1, color);
            break;
        case 3:
            DrawHorizontalLine(0, xStart, xStart + width, color);
            DrawHorizontalLine(4, xStart, xStart + width, color);
            DrawHorizontalLine(7, xStart, xStart + width, color);
            DrawVerticalLine(xStart + endCol, 0, Height, color);
            break;
        case 4:
            DrawVerticalLine(xStart, 3, Height, color);
            SetColor(xStart + 1, 3, color);
            DrawVerticalLine(xStart + endCol, 0, Height, color);
            break;
        case 5:
            DrawHorizontalLine(Height - 1, xStart, xStart + width, color);
            SetColor(xStart + 1, 4, color);
            DrawVerticalLine(xStart, 4, Height - 1, color);
            DrawVerticalLine(xStart + endCol, 1, 4, color);
            DrawHorizontalLine(0, xStart, xStart + width - 1, color);
            break;
        case 6:
            DrawHollowRectangle(xStart, 0, xStart + width, 5, color);
            DrawVerticalLine(xStart, 5, Height, color);
            DrawHorizontalLine(Height - 1, xStart + 1, xStart + width, color);
            break;
        case 7:
            DrawHorizontalLine(Height - 1, xStart, xStart + width, color);
            DrawVerticalLine(xStart, 0, 2, color);
            DrawVerticalLine(xStart + 1, 2, 5, color);
            DrawVerticalLine(xStart + endCol, 5, Height, color);
            break;
        case 8:
            DrawHollowRectangle(xStart, 0, xStart + width, Height, color);
            SetColor(xStart + 1, 4, color);
            break;
        case 9:
            DrawHollowRectangle(xStart, 3, xStart + width, Height, color);
            DrawHorizontalLine(0, xStart, xStart + width, color);
            DrawVerticalLine(xStart + endCol, 0, 3, color);
            break;
        default:
            ArgumentOutOfRangeException.ThrowIfLessThan(digit, 0);
            ArgumentOutOfRangeException.ThrowIfGreaterThanOrEqual(digit, 10);
            throw new NotImplementedException($"Drawing digit {digit} is not yet implemented!");
        }
    }
    public void DrawNumber(int number, TColor color) {
        if (number is < 0) logger.LogError("Cannot draw negative number (number was {number})", number);
        else if (number < 10) DrawDigit(number, 2, color);
        else if (number < 100) {
            DrawDigit(number / 10, 0, color);
            DrawDigit(number % 10, 4, color);
        } else logger.LogError("Cannot draw number larger than 100 (number was {number})", number);
    }
}
